# upload state file S3
terraform {
	backend "s3" {
		region 	= "ap-south-1"
		bucket 	= "s3-tf-statelist"
		key 	= "terraform.tfstate"
	}
}