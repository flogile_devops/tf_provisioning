#we need to add required field sets
provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
  #profile	 = "myprofile"
}

resource "aws_instance" "Testinstance" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"
  security_groups = ["${aws_security_group.allow_rdp.name}"]
  
  tags = {
    Name = "${var.Instance_name}"  
  }
  root_block_device {
    delete_on_termination = "true"
  }
}

resource "aws_security_group" "allow_rdp" {
  name        = "${var.Instance_name}"
  description = "Allow rdp traffic"

  ingress {
    from_port   = "${var.from_port}"
    to_port     = "${var.to_port}"
    protocol    = "${var.protocol}"
    cidr_blocks =  ["0.0.0.0/0"]
   }
   
   tags = {
    Name = "vpc-allow_rdp"  
  }
}

output "Instance_ip" {
  value = aws_instance.Testinstance.public_ip
}

output "uniqid" {
 value = "${var.Instance_name}"
}
