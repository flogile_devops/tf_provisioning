#provider
variable "access_key" {}
variable "secret_key" {}
variable "region" {
default="ap-south-1"
}

#resource-aws_instance-example
variable "ami" {
default="ami-0bcf5425cdc1d8a85"
}
variable "instance_type" {
default="t2.micro"
}
variable "Instance_name" {} 

#resource "aws_security_group" "allow_rdp"
##variable "name" {
#default="sgallow_rdp"
#}
variable "from_port" {
default=22
}
variable "to_port" {
default=22
}
variable "protocol" {
default="tcp"
}

# resource "aws_ebs_volume" "data-vol"
#variable "availability_zone" {}
variable "size" {
default=1
}

# resource "aws_volume_attachment"
variable "device_name" {
default="/dev/sdc"
}
